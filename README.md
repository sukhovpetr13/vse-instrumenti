environment
============
ubuntu18  
install python3.7  
install pip3  

setup
============
pip3 install selenium  
pip3 install allure  
pip3 install requests  

run
============
git pull https://gitlab.com/sukhovpetr13/vse-instrumenti.git  
pytest  

test comple
============
helper.py - main file with helpers  
